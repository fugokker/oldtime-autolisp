; The file is encoded in ANSI CP-1251.
; Copyright 2005 by Muravev Kirill <fugokker@yandex.ru>. Licensed under CC BY-SA 4.0

; Список экземпляров внешних ссылок, которые нужно обработать
(princ "\nЗагрузка модуля PImpXRef...")
(setq pimp_xp_objects nil)
; (princ "Удаление старых реакторов...")
; Удаление старых реакторов, если они есть
(foreach reactor_name (list
			'pimp_xp_xref_reactor
			'pimp_xp_acdb_reactor
			'pimp_xp_cmd_reactor
		      )
  (if (eval reactor_name)
    (progn
      (vlr-remove (eval reactor_name))
      (set reactor_name nil)
    ); progn
  ); if
); foreach
;(princ " Завершено.\nВыполнение остальных команд модуля...")

; Определяем реактор вставки внешней ссылки
(if (not pimp_xp_xref_reactor)
  (setq pimp_xp_xref_reactor (vlr-xref-reactor
			       nil
			       '(
				  (:vlr-beginAttach . pimp_xp_begin_attach)
				  (:vlr-abortAttach . pimp_xp_abort_attach)
				); '
			     ); vlr-xref-reactor
  ); setq
); if

; Определяем реактор базы данных
(if (not pimp_xp_acdb_reactor)
  (setq pimp_xp_acdb_reactor (vlr-acdb-reactor
			       nil
			       '(
				  (:vlr-objectAppended . pimp_xp_acdb_append)
				); '
			     ); vlr-acdb-reactor
  ); setq
); if

; Определяем командный реактор
(if (not pimp_xp_cmd_reactor)
  (setq pimp_xp_cmd_reactor (vlr-command-reactor
			      nil
			      '(
				 (:vlr-commandEnded . pimp_xp_cmd_end)
			       ); '
			    ); vlr-command-reactor
  ); setq
); if

; Процедура, обрабатывающая начало вставки внешней ссылки в базу данных документа
(defun pimp_xp_begin_attach (reactor
			     src_list)
  ; Флаг срабатывания реактора
  (vlr-data-set pimp_xp_acdb_reactor T)
); pimp_xp_begin_attach

; Процедура, обрабатывающая отмену вставки внешней ссылки в базу данных документа
(defun pimp_xp_abort_attach (reactor
			     src_list)
  ; Флаг срабатывания реактора
  (vlr-data-set pimp_xp_acdb_reactor nil)
); pimp_xp_abort_attach

; Процедура, обрабатывающая вставку экземпляра новой внешней ссылки в документ
(defun pimp_xp_acdb_append (reactor
			    src_obj /
			    obj)
  (if (vlr-data pimp_xp_acdb_reactor)
    (progn
      ; Присваиваем значение переменной obj с учётом возможности появления ошибки
      (setq obj (vl-catch-all-apply 'vlax-ename->vla-object (cdr src_obj)))
      (if (vl-catch-all-error-p obj)
	(setq obj nil)
      ); if
      ; Условия срабатывания реактора
      (if (and
	    obj
	    (= (vlax-get-property obj 'ObjectName) pimp_oname_BlockRef)
	  ); and
	; then
	(progn
	  ; Сбрасываем флаг срабатывания реактора
	  (vlr-data-set pimp_xp_acdb_reactor nil)
	  ; Задаём переменную
	  (setq pimp_xp_objects (append pimp_xp_objects (list obj)))
	  ; Задаём флаг срабатывания командного реактора
	  (vlr-data-set pimp_xp_cmd_reactor T)
	); progn
      ); if
    ); progn
  ); if
); pimp_xp_acdb_append

; Обработчик события завершения команды
(defun pimp_xp_cmd_end (reactor
			command_info /
			xref_doc
			collection
			copy_list
			obj_path)
  (if (vlr-data pimp_xp_cmd_reactor)
    (progn
      ; Сбрасываем флаг срабатывания реактора
      (vlr-data-set pimp_xp_cmd_reactor nil)
      ; Выполняем действия для каждого экземпляра внешних ссылок, вставляемых в документ
      (foreach obj pimp_xp_objects
	; Вспомогательные переменные
	(setq obj_path (vlax-get-property obj 'Path))
	; Задаём гиперссылку
	(princ "\nДобавление гиперссылки... ")
	(vlax-invoke-method
	  (vlax-get-property
	    obj
	    'Hyperlinks
	  ); vlax-get-property
	  'Add
	  obj_path
	); vlax-invoke-method
	(princ "OK.\n")
	(princ)
      ); foreach
      ; Копирование объектов модельного пространства из внешней ссылки в активный документ.
      ; Запускается при соблюдении следующих условий:
      ;  - в документ вставляется одна внешняя ссылка;
      ;  - приложение работает в многодокументном режиме
      (if (and
	    (= (length pimp_xp_objects) 1)
	    (= (getvar 'SDI) 0)
	  ); and
	(progn
	  (setq collection (pimp_get_blocks))
	  (vlax-for block collection
	    (if (=
		  (vlax-get-property block 'IsXRef)
		  :vlax-true
	        ); =
	      ; then
	      (if (=
		    (vlax-get-property block 'Path)
		    obj_path
		  ); =
	        ; then
		(progn
		  ; Если путь к внешней ссылке относительный, то делаем его абсолютным
		  (if (dos_ispathrelative obj_path)
		    (setq obj_path (dos_absolutepath (strcat (pimp_get_active_doc_path) "\\") obj_path))
		  ); if
		  (if (dos_filep obj_path)
		    (progn
	              (princ "Копирование объектов в модельное пространство документа... ")
	              (vlax-invoke-method
		        (vlax-get-property (vlax-get-acad-object) 'Documents)
		        'Open
		        obj_path
		        (vlax-make-variant :vlax-true vlax-vbBoolean)
	              ); vlax-invoke-method
		    ); progn
		  ); if
		); progn
	      ); if
	    ); if
	  ); vlax-for
	  (if (dos_filep obj_path)
	    (progn
	      (setq collection (vlax-get-property (vlax-get-acad-object) 'Documents))
	      (vlax-for doc collection
	        (if (=
		      (vlax-get-property doc 'FullName)
		      obj_path
	            ); =
	          ; then
	          (setq xref_doc doc)
	        ); if
	      ); vlax-for
	      ; Модельное пространство документа внешней ссылки
	      (setq collection (vlax-get-property
			         (vlax-get-property
			           xref_doc
			           'Database
			         ); vlax-get-property
			         'ModelSpace
			       ); vlax-get-property
	      ); setq
	      ; Заполняем массив объектами
	      (vlax-for mspace_obj collection
	        ; Копируем только те объекты, которые можно будет переместить
	        (if (vlax-method-applicable-p mspace_obj 'Move)
	          ; then
	          (setq copy_list (append copy_list (list mspace_obj)))
	        ); if
	      ); vlax-for
	      ; Копируем объекты из открытого документа внешней ссылки в активный документ.
	      ; Полученный список скопированных объектов преобразуется сначала в safearray, а потом в list
	      (setq copy_list (vlax-safearray->list
			        (vlax-variant-value
			          (vlax-invoke-method
			            xref_doc
			            'CopyObjects
			            ; Список объектов модельного пространства внешней ссылки,
			            ; преобразованный сначала в safearray, а потом в variant
			            (vlax-make-variant
				      (vlax-safearray-fill
				        (vlax-make-safearray
				          vlax-vbObject
				          (cons 0 (- (length copy_list) 1))
				        ); vlax-make-safearray
				        copy_list
				      ); vlax-safearray-fill
			            ); vlax-make-variant
			            ; Объект - владелец копируемых объектов (модельное пространство активного документа)
			            (vlax-make-variant
			              (vlax-get-property
			                (pimp_get_acad_property 'Database)
			                'ModelSpace
			              ); vlax-get-property
			              vlax-vbObject
			            ); vlax-make-variant
			          ); vlax-invoke-method
			        ); vlax-variant-value
			      ); vlax-safearray->list
	      ); setq
	      ; Закрываем открытый документ внешней ссылки
	      (vlax-invoke-method
	        xref_doc
	        'Close
	        (vlax-make-variant :vlax-false vlax-vbBoolean)
	      ); vlax-invoke-method
	      ; Перемещаем скопированные объекты в точку вставки
	      (foreach copy_obj copy_list
	        ; Вызываем метод "Move" для каждого объекта
	        (vlax-invoke-method
	          copy_obj
	          'Move
	          ; Описание точки с координатами (0, 0, 0).
	          ; Список координат преобразуется сначала в safearray, потом в variant
	          (vlax-make-variant
	            (vlax-safearray-fill
		      (vlax-make-safearray
		        vlax-vbDouble
		        (cons 0 2)
		      ); vlax-make-safearray
		      (list 0.0 0.0 (pimp_get_acad_property 'ElevationModelSpace))
	            ); vlax-safearray-fill
	          ); vlax-make-variant
	          ; Точка вставки внешней ссылки
	          (vlax-get-property (car pimp_xp_objects) 'InsertionPoint)
	        ); vlax-invoke-method
	      ); foreach
	      (princ "OK.\n")
	      (princ)
	    ); progn
	  ); if
	); progn
      ); if
      ; Обнуляем список вставленных внешних ссылок
      (setq pimp_xp_objects nil)
    ); progn
  ); if
); pimp_xp_cmd_end

(princ " OK.\n")
