; The file is encoded in ANSI CP-1251.
; Copyright 2005 by Muravev Kirill <fugokker@yandex.ru>. Licensed under CC BY-SA 4.0

; Функции общего пользования
(princ "\nЗагрузка модуля PImpGlobals...")

(vl-load-com)

; Переменные

(setq pimp_uppercase_list (vl-string->list "ABCDEFGHIJKLMNOPQRSTUVWXYZАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"))

(setq acad_err_exit "quit / exit abort")

(setq pimp_dlg_select_bg_filename "Выбор файла группы блоков")
(setq pimp_dlg_all_files_str "Все файлы (*.*)|*.*|")

(setq pimp_err_wrong_filename "Неверное имя файла")
(setq pimp_err_wrong_pathname "Неверное имя каталога")
(setq pimp_err_file_read "Ошибка при чтении из файла")
(setq pimp_err_wrong_input "Неверный ввод")
(setq pimp_err_blocks_nf "В рисунке не найдены следующие блоки")

(setq pimp_msg_all "Все")
(setq pimp_msg_exit "выХод")
(setq pimp_msg_group "Группа")
(setq pimp_msg_select "выБрать")
(setq pimp_msg_bg_filename "Имя файла группы блоков")

;|(setq pimp_mtd_Add "ADD")
(setq pimp_mtd_Item "ITEM")|;

(setq pimp_oname_AttDef "AcDbAttributeDefinition")
(setq pimp_oname_Layer "AcDbLayerTableRecord")
(setq pimp_oname_BlockRef "AcDbBlockReference")

;|(setq pimp_prp_ActiveDocument "ACTIVEDOCUMENT")
(setq pimp_prp_ColorIndex "COLORINDEX")
(setq pimp_prp_Count "COUNT")
(setq pimp_prp_Dictionaries "DICTIONARIES")
(setq pimp_prp_FullName "FULLNAME")
(setq pimp_prp_Height "HEIGHT")
(setq pimp_prp_Hyperlinks "HYPERLINKS")
(setq pimp_prp_InsertionPoint "INSERTIONPOINT")
(setq pimp_prp_Layer "LAYER")
(setq pimp_prp_Linetype "LINETYPE")
(setq pimp_prp_Linetypes "LINETYPES")
(setq pimp_prp_LinetypeScale "LINETYPESCALE")
(setq pimp_prp_Lineweight "LINEWEIGHT")
(setq pimp_prp_Mode "MODE")
(setq pimp_prp_Name "NAME")
(setq pimp_prp_ObjectName "OBJECTNAME")
(setq pimp_prp_PlotStyleName "PLOTSTYLENAME")
(setq pimp_prp_PromptString "PROMPTSTRING")
(setq pimp_prp_TagString "TAGSTRING")
(setq pimp_prp_TextGenerationFlag "TEXTGENERATIONFLAG")
(setq pimp_prp_TextString "TEXTSTRING")
(setq pimp_prp_Thickness "THICKNESS")
(setq pimp_prp_TrueColor "TRUECOLOR")|;

; Имя словаря с необходимыми данными
(setq pimp_data_dict_name "PImpDictionary")
; Имя записи в словаре (имя файла группы блоков)
(setq pimp_xrecord_bg_filename "BlocksGroupFileName")
; Имя записи в словаре (имя файла порядка сортировки атрибутов группы блоков)
(setq pimp_xrecord_bgo_filename "AttributeOrderFileName")
; Каталог для сохранения блоков
(setq pimp_xrecord_wblock_dir "WBlockDirectory")

;|(setq pimp_var_clayer "CLAYER")
(setq pimp_var_deflplstyle "DEFLPLSTYLE")
(setq pimp_var_filedia "FILEDIA")
(setq pimp_var_pstylemode "PSTYLEMODE")|;

; Функции

; Обработчик ошибок, не сообщающий о выходе из процедур
;|(defun *error* (msg)
  (if (/= msg acad_err_exit)
    (progn
      (princ "error: ")
      (princ msg)
    ); progn
  ); if
  (princ)
); *error*
|;

; Возвращает ссылку на приложение AutoCAD Mechanical
;|(defun pimp_get_mcad_object ()
  (vlax-invoke-method (vlax-get-acad-object) 'GetInterfaceObject "MCAD.Application")
); pimp_get_mcad_object|;


; Возвращает активный документ AutoCAD
(defun pimp_get_active_doc ()
  (vlax-get-property (vlax-get-acad-object) 'ActiveDocument)
); pimp_get_active_doc


; Возвращает активный документ AutoCAD Mechanical
;|(defun pimp_get_mcad_doc ()
  (vlax-get-property (pimp_get_mcad_object) 'ActiveDocument)
); pimp_get_mcad_doc|;


; Возвращает указанное свойство активного документа
; Аргумент "name" может быть типа STR или SYM
(defun pimp_get_acad_property (name)
  (vlax-get-property (pimp_get_active_doc) name)
); pimp_get_acad_property


; Возвращает путь к активному документу без косой черты в конце
(defun pimp_get_active_doc_path ( / doc_path)
  (setq doc_path (vlax-get-property (pimp_get_active_doc) 'Path))
  (if (=
	(substr
	  doc_path
	  (strlen doc_path)
	); substr
	"\\"
      ); =
    ; then
    (substr doc_path 1 (- (strlen doc_path) 1))
    doc_path
  ); if
); defun


; Возвращает коллекцию блоков рисунка
(defun pimp_get_blocks ()
  (pimp_get_acad_property 'Blocks)
); pimp_get_blocks


; Возвращает список имён всех блоков рисунка (кроме *ModelSpace и *PaperSpace)
(defun pimp_get_block_names ( / blocks result)
  (setq blocks (pimp_get_blocks))
  (vlax-for block blocks
    (setq result (append result (list (vlax-get-property block 'Name))))
  ); vlax-for
  (cddr result)
); pimp_get_block_names


; Возвращает полный список параметров LISP объекта VLA
(defun pimp_vla2lisp (obj)
  (entget (vlax-vla-object->ename obj))
); pimp_vla2lisp


; Возвращает VLA-объект словаря с заданным именем (если есть)
(defun pimp_get_dict (dict_name /
		      cur_dict
		      dicts_collection
		      i
		      count
		      found_flag)
  (setq dicts_collection (pimp_get_acad_property 'Dictionaries)
	count (vlax-get-property dicts_collection 'Count)
	i 0
  )
  (while (and (not found_flag) (< i count))
    (setq cur_dict (vlax-invoke-method dicts_collection 'Item i))
    (if (vlax-property-available-p cur_dict 'Name)
      (setq found_flag (= dict_name (vlax-get-property cur_dict 'Name)))
    )  
    (setq i (1+ i))
  )
  (if found_flag
    cur_dict
    nil
  )
); pimp_get_dict


; Создаёт новый словарь в документе
(defun pimp_create_dict (name)
  (vlax-invoke-method (pimp_get_acad_property 'Dictionaries) 'Add name)
); pimp_create_dict


; Добавляет PlotStyleName к списку имён свойств объекта (если это возможно)
(defun pimp_append_pstylename (source_list)
  (if (= (getvar 'pstylemode) 0)
    (append source_list (list 'PlotStyleName))
    source_list
  )
); pimp_append_pstylename


; Добавляет в конец имени каталога символ "\"
(defun pimp_correct_pathname (pathname)
  (strcat (vl-string-right-trim "\\" pathname) "\\")
); pimp_correct_pathname


; Читает список строк из файла
(defun pimp_get_list_from_file (filename / file
			                   cur_str
			                   result)
  ; Открываем файл
  (setq file (open filename "r"))
  (if (null file)
    (progn
      (*error* (strcat pimp_err_file_read " """ filename """"))
      (exit)
    )
  )
  ; Предварительное значение текущей строки
  (setq cur_str "")
  ; Последовательно читаем все строки из файла
  (while (/= cur_str nil)
    ; Читаем следующую строку
    (setq cur_str (read-line file))
    ; Добавляем строку в файл или закрываем его
    (if (/= cur_str nil)
      (setq result (append result (list cur_str)))
      (close file)
    )
  )
  ; Возвращаем результат
  result
); pimp_get_list_from_file

; Строка приглашения для ввода значений из командной строки
(defun pimp_prompt_str (msg_str
			kwords_list
			default_kword /
			msg)
  ; Формируем строку сообщения
  (if (or (null msg_str) (= msg_str ""))
    (setq msg "\n")
    (setq msg (strcat "\n" msg_str))
  )
  ; Добавляем к сообщению список ключевых слов
  (if (and kwords_list (listp kwords_list))
    (progn
      (if (/= msg "\n")
        (setq msg (strcat msg " "))
      )
      (setq msg (strcat msg "["))
      (foreach kword kwords_list
        (setq msg (strcat msg kword "/"))
      )
      (setq msg (strcat (vl-string-right-trim "/" msg) "]"))
    )
  )
  ; Добавляем к сообщению ключевое слово по умолчанию
  (if (and default_kword (/= default_kword ""))
    (setq msg (strcat msg " <" default_kword ">"))
  )
  ; Добавляем к сообщению двоеточие
  (strcat msg ": ")
); pimp_prompt_str


; Выбор опции в командной строке

			    ; текст сообщения
(defun pimp_select_keyword (msg_str
			    ; список ключевых слов или NIL
			    kwords_list
			    ; Ключевое слово по умолчанию
			    ; (может не содержаться в списке)
			    default_kword /
			    kwords_str
			    msg)
  (setq msg (pimp_prompt_str msg_str kwords_list default_kword)
        kwords_str ""
  )
  ; Формируем строку списка ключевых слов
  (if (and kwords_list (listp kwords_list))
    (progn
      (foreach kword kwords_list
        (setq kwords_str (strcat kwords_str kword " "))
      )
      (setq kwords_str (vl-string-right-trim " " kwords_str))
    )
  )
  (if (or (null default_kword) (= default_kword ""))
    (initget 1 kwords_str)
    (initget kwords_str)
  )
  ; Возврат результата с учётом возможности выбора слова по умолчанию
  (setq msg (getkword msg))
  (if (null result)
    default_kword
    msg
  )
); pimp_select_keyword


; Возвращает введённую с клавиатуры строку
; Введённая строка сравнивается со списком ключевых слов.
; Если строка так или иначе входит в этот список, то возвращается ключевое слово.
; Если строка не входит в список, то возвращается она сама

		      ; Строка приглашения
(defun pimp_readline (msg_str
		      ; Список ключевых слов или NIL
		      kwords_list
		      ; Ключевое слово по умолчанию или NIL
		      default_kword
		      ; Можно ли сокращать слово по умолчанию (T/nil)
		      default_kword_has_alias /
		      full_kwords_list
		      result)

  ; Возвращает аббревиатуру ключевого слова
  (defun _make_alias (word / alias)
    (setq alias "")
    (foreach code (vl-string->list word)
      (if (member code pimp_uppercase_list)
        (setq alias (strcat alias (vl-list->string (list code))))
      ); if
    ); foreach
    (list word alias)
  ); _make_alias

  (prompt (pimp_prompt_str msg_str kwords_list default_kword))
  (setq result (read-line))
  ; Формируем полный список ключевых слов из списка kwords_list
  (if (and kwords_list (listp kwords_list))
    (foreach kword kwords_list
      (setq full_kwords_list (append full_kwords_list (list (_make_alias kword))))
    ); foreach
  ); if
  ; Формируем полный список ключевых слов из переменной default_kword
  (if (and
        default_kword
        (/= default_kword "")
        (null (member default_kword kwords_list))
      ); and
    (setq full_kwords_list (append
			     full_kwords_list
			     (list
			       (if default_kword_has_alias
			         (_make_alias default_kword)
			         (list default_kword default_kword)
			       ); if
			     ); list
			   ); append
    ); setq
  ); if
  ; Сверяем введённое значение с полным списком ключевых слов
  (if full_kwords_list
    (foreach kword full_kwords_list
      (if (or
	    (= (strcase result) (strcase (nth 0 kword)))
	    (= (strcase result) (nth 1 kword))
	  ); or
	(setq result (car kword))
      ); if
    ); foreach
  ); if
  ; Особый случай
  (if (and default_kword (= result ""))
    (setq result default_kword)
  ); if
  result
); pimp_readline


; Ввод имени файла или ключевого слова
; Минимальный список ключевых слов: [Выбрать.../выХод]
; По умолчанию в приглашение подставляется параметр default_filename (если задан), либо "выХод"
; Возвращает введённое имя файла или ключевое слово (кроме "Выбрать...")

			    ; Сообщение (STR или NIL)
(defun pimp_input_filename (msg
			    ; Список дополнительных ключевых слов или (LIST, STR, NIL)
			    additional_kwords_list
			    ; Имя файла по умолчанию (LIST, STR, NIL)
			    default_filename
			    ; Список уже введённых имён файлов (LIST, STR, NIL)
			    old_filenames
			    ; Заголовок диалогового окна (STR или NIL)
			    dlg_title
			    ; Расширение файла для диалогового окна (STR или NIL)
			    dlg_ext
			    ; Флаги диалогового окна (см. пояснение к функции getfiled)
			    ; бит 1:
			    ; 0 - выбор файла
			    ; 1 - выбор каталога
			    dlg_flags /
			    kwords_list
			    member_flag
			    correct_input_flag
			    result)
  (setq err_incorrect_parameter "pimp_input_filename - неверное имя параметра ")
  ; Минимальный список ключевых слов
  (setq kwords_list (list pimp_msg_select pimp_msg_exit))
  ; Исправляем в параметрах диалогового окна NIL на пустую строку
  (foreach var (list
		 'default_filename
		 'dlg_title
	       ); list
    (if (null (eval var))
      (set var "")
    ); if
  ); foreach
  (if (= additional_kwords_list "")
    (setq additional_kwords_list nil)
  ); if
  ; Формируем полный список ключевых слов
  (cond
    ((or
       (null additional_kwords_list)
       (= (type additional_kwords_list) 'LIST)
     ); or
      (setq kwords_list (append additional_kwords_list kwords_list))
    );
    ((and
       (= (type additional_kwords_list) 'STR)
       (/= additional_kwords_list "")
     )
      (setq kwords_list (append (list additional_kwords_list) kwords_list))
    );
    (T
      (*error* (strcat err_incorrect_parameter "additional_kwords_list"))
      (exit)
    );
  ); cond
  ; Запрашиваем ввод до тех пор, пока не будет введено правильное имя файла
  (while (null correct_input_flag)
    ; Запрос ввода имени файла
    (setq result (pimp_readline
		  msg
		  kwords_list
		  (if (= default_filename "")
		    pimp_msg_select
		    default_filename
		  )
		  (/= default_filename "")
	         ); pimp_readline
    ); setq
    ; Если ввели ключевое слово, то выходим
    ; В противном случае проверяем правильность ввода имени файла
    (setq member_flag (not (null (member result kwords_list))))
    (cond
      ; Если скомандовали вызвать диалоговое окно
      ((= result pimp_msg_select)
        (cond
	  ; Если выбираем файл
	  ((= (logand dlg_flags 2) 0)
	    (setq result (getfiled
			   dlg_title
			   default_filename
			   dlg_ext
			   dlg_flags
			 ); getfiled
	    ); setq
	  )
	  ; Если выбираем каталог
	  ((/= (logand dlg_flags 2) 0)
	    (setq result (dos_getdir
			   dlg_title
			   default_filename
			   ""
			   (/= (logand dlg_flags 1) 0)
			 ); dos_getdir
	    ); setq
	  )
	); cond
        (if result
          (setq default_filename result)
	)
      ) 
      ; Если просто ввели имя файла
      ((not member_flag)
        (cond
	  ; Проверяем наличие файла
	  ((= (logand dlg_flags 2) 0)
	    (setq correct_input_flag (or
				       (/= (logand dlg_flags 1) 0)
				       (dos_filep result)
				     ); and
		  
	    ); setq
	  );
	  ; Проверяем наличие каталога
	  ((/= (logand dlg_flags 2) 0)
	    (setq result (pimp_correct_pathname result)
		  correct_input_flag (dos_dirp result)
	    ); setq
	  );
	); cond
        (if (null correct_input_flag)
          (progn
	    (setq result nil)
	    (princ "\n")
	    ; Сообщение об ошибке в случае, если файл не найден
	    (cond
	      ; Проверяем наличие файла
	      ((= (logand dlg_flags 2) 0)
		(princ pimp_err_wrong_filename)
	      );
	      ; Проверяем наличие каталога
	      ((/= (logand dlg_flags 2) 0)
		(princ pimp_err_wrong_pathname)
	      );
	    ); cond
	  ); progn
	); if
      )
      (member_flag
	(setq correct_input_flag T)
      )
    ); cond
    ; Проверяем, было ли имя файла уже введено ранее
    (if (and (not member_flag) correct_input_flag)
      (progn
	(cond
	  ((and (= (type old_filenames) 'STR) (/= old_filenames ""))
	    (setq correct_input_flag (/= (strcase result) (strcase old_filenames)))
	  )
	  ((= (type old_filenames) 'LIST)
	    (foreach old_name old_filenames
	      (setq correct_input_flag (and
					 correct_input_flag
					 (/=
					   (strcase result)
					   (strcase old_name)
				         ); =
				       ); and
	      ); setq
	    ); foreach
	  )
	); cond
	(if (not correct_input_flag)
	  (princ "\nЭто имя уже введено!")
	); if
      ); progn
    ); if
  ); while
  result
); pimp_input_filename
(princ " OK.\n")
