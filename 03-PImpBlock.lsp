; The file is encoded in ANSI CP-1251.
; Copyright 2005 by Muravev Kirill <fugokker@yandex.ru>. Licensed under CC BY-SA 4.0

; Сортировка атрибутов в группе блоков
(princ "\nЗагрузка модуля PImpBlock...")

; Список имён атрибутов блока
;|(defun pimp_get_att_tags (block_name /
			  block
			  result)
	; Определение нужного блока
  (setq block (vlax-invoke-method (pimp_get_blocks) pimp_mtd_Item block_name))
  (vlax-for cur_obj block
    (if (= (vlax-get-property cur_obj pimp_prp_ObjectName) pimp_oname_AttDef)
      ; Добавляем к результату имя найденного атрибута
      (setq result (append result (list (vlax-get-property cur_obj pimp_prp_TagString))))
    )
  )
); pimp_get_att_tags|;

; Сортирует список имён блоков рисунка по алфавиту и записывает его в текстовый файл
;|(defun pimp_export_block_names (filename /
			        file
			        blocks_list
			        result)
  (setq file (open filename "w"))
  (if (null file)
    (progn
      (princ (strcat "\nНевозможно открыть файл " filename))
      (exit)
    ); progn
  ); if
  ; Список имён блоков с сортировкой по алфавиту
  (setq blocks_list (vl-sort (pimp_get_block_names) '<))
  ; Записываем список в файл
  (foreach bname blocks_list
    (write-line bname file)
  ); foreach
  ; Закрываем файл
  (setq result (close file))
  ; Возвращаем результат
  (if (null result)
    T
    result
  ); if
); pimp_export_block_names|;


; Проверяет наличие в базе данных блоков с именами из файла
; В случае несоответствия выводит список имён блоков, не найденных в рисунке
; Результат выводится как (T) - в случае удачной проверки,
; или (nil <name1> <name2>...) - если проверка не прошла

				; список имён блоков группы
;|(defun pimp_check_blocks_group (group_list /
				blocks_list
			        result)
  ; Коллекция блоков
  (setq blocks_list (pimp_get_block_names))
  ; Сравниваем списки
  (foreach name group_list
    (if (not (member name blocks_list))
      (setq result (append result (list name)))
    )
  )
  ; В случае совпадения возвращаем T
  (if result
    (append (list nil) result)
    (eval '(list T))
  )
); pimp_check_blocks_group|;


; Проверяет соответствие состава атрибутов группы блоков образцу
; В случае несоответствия выводит список ошибочных блоков
; Результат выводится как (T) - в случае удачной проверки,
; или (nil <name1> <name2>...) - если проверка не прошла
			     ; список имён блоков группы
;|(defun pimp_check_att_order (group_list
			     ; упорядоченный список атрибутов
			     order_list /
			     x_list
			     att_tags
			     unique_flag
			     result)
  ; Перебор всей группы блоков
  (foreach gb_name group_list
    ; Извлекаем список имён атрибутов текущего блока группы
    (setq att_tags (pimp_get_att_tags gb_name))
    ; Если количество атрибутов блока и образца разное, то выходим из цикла
    (if (/= (length att_tags) (length order_list))
      ; then
      ; Формируем список неверных имён блоков
      (setq x_list (append x_list (list gb_name)))
      ; else
      ; Если количества атрибутов совпадают, выполняем дополнительную проверку
      (progn
	(setq unique_flag T)
	(foreach tag att_tags
	  (if (and
		(not (member tag order_list))
		unique_flag
	      )
	    (setq x_list (append x_list (list gb_name))
	          unique_flag nil
	    )
	  ); if
	); foreach
      ); progn
    ); if
  )
  ; Выводим результат функции
  (setq result (list (null x_list)))
  (if (not (null x_list))
    (setq result (append result x_list))
  )
  result
); pimp_check_att_order|;


; Двухуровневый список свойств заданного атрибута
			   ; VLA-объект (атрибут блока)
;|(defun pimp_get_att_props (source /
			   prop_names
			   result)
  ; Проверку типа аргумента не производим
  ; Список доступных для записи свойств атрибута
  (setq prop_names (list
		     "Alignment"
		     "Backward"
		     "Constant"
		     "FieldLength"
		     pimp_prp_Height
		     pimp_prp_InsertionPoint
		     "Invisible"
		     pimp_prp_Layer
		     pimp_prp_Linetype
		     pimp_prp_LinetypeScale
		     pimp_prp_Lineweight
		     "LockPosition"
		     pimp_prp_Mode
		     "Normal"
		     "ObliqueAngle"
		     ;"PlotStyleName"
		     "Preset"
		     pimp_prp_PromptString
		     "Rotation"
		     "ScaleFactor"
		     "StyleName"
		     pimp_prp_TagString
		     "TextAlignmentPoint"
		     ;TextGenerationFlag
		     pimp_prp_TextString
		     pimp_prp_Thickness
		     pimp_prp_TRUECOLOR
		     "UpsideDown"
		     "Verify"
		     "Visible"
		   ); list
  ); setq
  ; Если используются именованные стили печати,
  ; то добавляем к списку свойсво PlotStyleName
  (setq prop_names (pimp_append_pstylename prop_names))
  ; Свойства гиперссылки
  ; Формируем совмещённый список имён и значений свойств атрибута
  (foreach prop_name prop_names
    (setq result (append result (list (list prop_name (vlax-get-property source prop_name)))))
  ); foreach
); pimp_get_att_props|;


; Выстраивает атрибуты блоков заданной группы в заданном порядке
				; список имён блоков группы
;|(defun pimp_reorder_attributes (group_list
				; упорядоченный список атрибутов
				order_list /
				i
				count
				blocks
				block
				att_list
				hl_list
				hl_prop_names
				new_hl
				obj
				entity
				text_gen_flag)
  (setq hl_prop_names (list
			pimp_prp_Name
			"URL"
			"URLDescription"
			"URLNamedLocation"
		      ); list
	count (length hl_prop_names)
	i 1
  ); setq
  ; Коллекция блоков
  (setq blocks (pimp_get_blocks))
  ; Выполняем поиск в каждом блоке из группового списка
  (foreach block_name group_list
    ; Текущий блок
    (setq block (vlax-invoke-method blocks pimp_mtd_Item block_name)
          hl_list nil
          att_list nil
	  text_gen_flag nil
    )
    ; Выполняем поиск по всем тагам
    (foreach tag order_list
      (vlax-for element block
	(if (and
	      (=
		(vlax-get-property element pimp_prp_ObjectName)
		pimp_oname_AttDef
	      )
	      (=
		(vlax-get-property element pimp_prp_TagString)
		tag
	      )
	    )
          (progn
	    	  ; Формируем список свойств атрибута
	    (setq att_list (append att_list (list (pimp_get_att_props element)))
		  ; Отдельно запоминаем свойство атрибута TextGenerationFlag
		  text_gen_flag (vlax-get-property element pimp_prp_TextGenerationFlag)
	    )
	    ; Запоминаем гиперссылки
	    (vlax-for link (vlax-get-property element pimp_prp_Hyperlinks)
              (setq new_hl nil)
	      (foreach link_property hl_prop_names
		(setq new_hl (append new_hl (list (vlax-get-property link link_property))))
	      ); foreach
	      (setq hl_list (append hl_list (list new_hl)))
	    ); vlax-for
	    ; Стираем атрибуты
            (vlax-invoke-method element pimp_mtd_Delete)
	    (vlax-release-object element)
	  )
	); if
      ); vlax-for
    ); foreach
    
    ; Создаём атрибуты заново в нужном порядке
    (foreach att_record att_list
      (setq obj (vlax-invoke-method
                  block
                  "AddAttribute"
		  (cadr (assoc pimp_prp_Height att_record))
		  (cadr (assoc pimp_prp_Mode att_record))
		  (cadr (assoc pimp_prp_PromptString att_record))
		  (cadr (assoc pimp_prp_InsertionPoint att_record))
		  (cadr (assoc pimp_prp_TagString att_record))
		  (cadr (assoc pimp_prp_TextString att_record))
	        )
      )
      ; Копируем свойства
      (foreach att_pair att_record
        (vlax-put-property obj (car att_pair) (cadr att_pair))
      )
      ; Вставляем гиперссылки
      (foreach link hl_list
	; Добавляем объект
	(setq new_hl (vlax-invoke-method (vlax-get-property obj pimp_prp_Hyperlinks) pimp_mtd_Add (car link)))
	; Присваиваем новому объекту свойства
	(while (< i count)
	  (vlax-put-property new_hl (nth i hl_props_list) (nth i link))
	  (setq i (1+ i))
	); while
      ); foreach
      ; Отдельно через LISP задаём значение свойства TextGenerationFlag
      (setq entity (pimp_vla2lisp obj)
	    entity (subst
		     (cons 71 text_gen_flag)
		     (assoc 71 entity)
		     entity
		   )
      )
      (entmod entity)
    ); foreach
  ); foreach
); pimp_reorder_attributes|;


; Изменение значения общего свойства объектов для группы блоков
;|(defun pimp_change_general_property (group_list
		                     prop_name
		                     prop_value
		                     prop_to_change
		                     new_value /
		                     available_props
				     prop_access_flag
				     rgb_value
				     i
		                     blocks
		                     block
				     blocks_count
				     obj_count)

  ; Цвет графического объекта
  (defun _get_color_obj (obj)
    (vlax-get-property obj pimp_prp_TrueColor)
  )

  ; Индекс цвета объекта
  (defun _get_color_index (obj)
    (vlax-get-property (_get_color_obj obj) pimp_prp_ColorIndex)
  )

  ; Список значений RGB цвета объекта
  (defun _get_rgb (obj / result)
    (foreach name (list "Red" "Green" "Blue")
      (setq result (append result (list (vlax-get-property (_get_color_obj obj) name))))
    )
  )

        ; Доступные для записи свойства
  (setq available_props (pimp_append_pstylename
			  (list
	                    pimp_prp_Layer
	                    pimp_prp_Linetype
	                    pimp_prp_LinetypeScale
	                    pimp_prp_Lineweight
	                    pimp_prp_Thickness
			    pimp_prp_TrueColor
	                  )
			)
  )
  ; Проверка правильности ввода имён свойств
  (foreach name (list prop_name prop_to_change)
    (if (not (member (strcase name) available_props))
      (progn
        (princ (strcat "\nНевозможно изменить свойство """ (strcase name) """ для всех объектов группы!"))
        (exit)
      )
    )
  )
  (setq blocks (pimp_get_blocks)
	blocks_count 0
	obj_count 0
  )
  ; Замена свойств
  (foreach name group_list
    (setq block (vlax-invoke-method blocks pimp_mtd_Item name))
    (vlax-for obj block
      (if (and
	    (vlax-property-available-p obj prop_name)
	    (vlax-property-available-p obj prop_to_change T)
	  )
	(progn
          (setq prop_access_flag nil)
          ; Если выбран цвет объекта
          (cond
            ((= (strcase prop_name) pimp_prp_TrueColor)
              (cond
	        ; Если цвет задан индексом
                ((= (type prop_value) 'INT)
	          (setq prop_access_flag (= prop_value (_get_color_index obj)))
	        )
	        ; Если цвет задан списком RGB
                ((and (= (type prop_value) 'LIST) (= (length prop_value) 3))
	          (setq prop_access_flag T
		        rgb_value (_get_rgb obj)
		        i 0
	          )
	          (while (< i 3)
	            (setq prop_access_flag (and
			         	     prop_access_flag
				             (= (nth i prop_value) (nth i rgb_value))
				           )
		          i (1+ i)
	            ); setq
	          ); while
	        )
	        (T
	          (setq prop_access_flag nil) 
	        )
              ); cond
	    ) 
	    ; Если выбрано другое свойство
	    (T
	      (cond
	        ((= (type prop_value) 'STR)
	          (setq prop_access_flag (= (strcase prop_value) (strcase (vlax-get-property obj prop_name))))
	        )
	        (T
	          (setq prop_access_flag (= prop_value (vlax-get-property obj prop_name)))
	        )
	      ); cond
	    )
          ); cond
	  ; Замена свойства
	  ; Меняем свойство только в том случае, если оно существует и доступно для записи
	  (if prop_access_flag
	    (cond
  	      ; Если изменяется цвет объекта
	      ((= (strcase prop_to_change) 'TrueColor)
	        (cond
		  ((= (type new_value) 'INT)
		    (vlax-put-property (_get_color_obj obj) 'ColorIndex new_value)
	            (setq obj_count (1+ obj_count))
		  )
		  ((and (= (type new_value) 'LIST) (= (length new_value) 3))
		    (vlax-invoke-method
		      (_get_color_obj obj)
		      'SetRGB
		      (nth 0 new_value)
		      (nth 1 new_value)
		      (nth 2 new_value)
		    ); vlax-invoke-method
	            (setq obj_count (1+ obj_count))
		  )
	        ); cond
	      )
  	      ; Если изменяется другое свойство
	      (T
                (vlax-put-property obj prop_to_change new_value)
	        (setq obj_count (1+ obj_count))
	      )
	    ); cond
	  ); if
	); progn
      ); if
    ); vlax-for
    (setq blocks_count (1+ blocks_count))
  ); foreach
  (list blocks_count obj_count)
); pimp_change_general_property|;


; Команда перупорядочивания атрибутов
;|(defun c:attreorder ( /
		     group
		     order
		     dictionary
		     function_call_list
		     err_result)

  ; Ввод имён двух файлов
  (defun _inner_select ( /
			select_flag)
    (while (null select_flag)
            ; Ввод имени файла группы блоков
      (setq group (pimp_input_filename
		    pimp_msg_bg_filename
		    pimp_msg_all
		    group
		    nil
		    pimp_dlg_select_bg_filename
		    ""
		    0
		  )
      ); setq
      (if (= group pimp_msg_exit)
	(exit)
      ); if
      ; Ввод имени файла сортировки атрибутов
      (setq order (pimp_input_filename
		    "Имя файла сортировки атрибутов"
		    pimp_msg_group
		    order
		    group
		    "Выбор файла сортировки атрибутов"
		    ""
		    0
		  ); pimp_input_filename
      ); setq
      (cond
	((= order pimp_msg_exit)
	  (exit)
	);
	((= order pimp_msg_group)
          (setq select_flag nil
	        order nil
	  ); setq
	);
	(T
	  (setq select_flag T)
	);
      ); cond
    ); while
  ); _inner_select

  ; Извлекаем ранее записанные имена файлов из словаря
  (setq dictionary (pimp_get_dict pimp_data_dict_name))
  (if dictionary
    (progn
      (setq group (vlax-ldata-get dictionary pimp_xrecord_bg_filename nil)
	    order (vlax-ldata-get dictionary pimp_xrecord_bgo_filename nil)
      ); setq
    ); progn
  ); if
  ; Ввод данных с учётом возможности прерывания команды
  (if (vl-catch-all-error-p
        (vl-catch-all-apply '_inner_select nil)
      ); vl-catch-all-error-p
    ; Выход в случае ошибки
    (exit)
  ); if
  ; При необходимости создаём словарь
  (if (null dictionary)
    (setq dictionary (pimp_create_dict pimp_data_dict_name))
  ); if
  ; Заменяем значения записей словаря
  (vlax-ldata-put dictionary pimp_xrecord_bg_filename group)
  (vlax-ldata-put dictionary pimp_xrecord_bgo_filename order)
  ; Список параметров вызова функции проверки группы или порядка
  (setq function_call_list (list
			     (list 'pimp_check_att_order 'group 'order)
			   ); list
  ); setq
  ; Читаем списки строк из файлов
  (if (= group pimp_msg_all)
    ; then
    (setq group (pimp_get_block_names))
    ; else
    (progn
      (setq group (pimp_get_list_from_file (findfile group))
	    ; если список взят из файла, то включаем функцию его проверки
	    function_call_list (list
			         (list 'pimp_check_blocks_group 'group)
				 (car function_call_list)
			       ); list
      ); setq
    ); progn
  ); if
  (setq order (pimp_get_list_from_file (findfile order)))
  ; Проверяем правильность списков
  (foreach args function_call_list
    (cond
      ((= (nth 0 args) 'pimp_check_blocks_group)
        (princ "\nВыполняется проверка файла группы...")
      );
      ((= (nth 0 args) 'pimp_check_att_order)
        (princ "\nВыполняется проверка файла сортировки...")
      );
    ); cond
    (setq err_result (eval args))
    (if (null (car err_result))
      ; then
      (progn
        (princ "\n")
	(cond
	  ((= (nth 0 args) 'pimp_check_blocks_group)
	    (princ (strcat pimp_err_blocks_nf ":"))
	  )
          ((= (nth 0 args) 'pimp_check_att_order)
	    (princ "Неверно указан состав атрибутов для следующих блоков:")
          )
	); cond
        (foreach wrong_block (cdr err_result)
	  (princ "\n")
	  (princ wrong_block)
	); foreach
        (exit)
      ); progn
      ; else
      (princ " OK.")
    ); if
  ); foreach
  (princ "\nВыполняется переупорядочивание атрибутов...")
  (pimp_reorder_attributes group order)
  (princ " OK.")
  (princ)
); c:attreorder|;


; Команда для смены значений свойств Linetype и Lineweight на BYBLOCK
; Применяется для всех объектов группы блоков, значение цвета у которых установлено как ByBlock
;|(defun c:macrobyblock ( /
		       dictionary
		       group_list
		       err_result)

  ; Ввод имени файла
  (defun _inner_select ()
    (setq group_list (pimp_input_filename
		       pimp_msg_bg_filename
		       pimp_msg_all
		       group_list
		       nil
		       pimp_dlg_select_bg_filename
		       ""
		       0
		     ); pimp_input_filename
    ); setq
    (if (= group_list pimp_msg_exit)
      (exit)
    ); if
  ); _inner_select
  
  (setq dictionary (pimp_get_dict pimp_data_dict_name))
  (if dictionary
    (setq group_list (vlax-ldata-get dictionary pimp_xrecord_bg_filename nil))
  ); if
  ; Ввод имени файла или выход
  (if (vl-catch-all-error-p
          (vl-catch-all-apply '_inner_select nil)
      ); vl-catch-all-error-p
    (exit)
  ); if
  ; При необходимости создаём словарь
  (if (null dictionary)
    (setq dictionary (pimp_create_dict pimp_data_dict_name))
  ); if
  ; Помещаем в словарь новое имя файла
  (vlax-ldata-put dictionary pimp_xrecord_bg_filename group_list)
  ; Читаем список строк
  (if (= group_list pimp_msg_all)
    ; then
    (setq group_list (pimp_get_block_names))
    ; else
    (progn
      (setq group_list (pimp_get_list_from_file (findfile group_list))
	    err_result (pimp_check_blocks_group group_list)
      ); setq
      (if (null (car err_result))
	(progn
	  (princ (strcat "\n" pimp_err_blocks_nf ":"))
	  (foreach err_name (cdr err_result)
	    (princ "\n")
	    (princ err_name)
	  ); foreach
	  (exit)
	); progn
      ); if
    ); progn
  ); if
  ; Проверяем состав группы
  (foreach prop_set (list
		      (list pimp_prp_Linetype "ByBlock")
		      (list pimp_prp_Lineweight -2)
		      (list pimp_prp_PlotStyleName "ByBlock")
		    )
    (princ (strcat "\nИзменение значения свойства " (nth 0 prop_set) "... "))
    (setq err_result (pimp_change_general_property
		       group_list
		       pimp_prp_TrueColor
		       0
		       (nth 0 prop_set)
		       (nth 1 prop_set)
		     )
    ); setq
    (princ (strcat "OK.\nКоличество просмотренных блоков: " (rtos (nth 0 err_result))))
    (princ (strcat "\nКоличество изменённых объектов: " (rtos (nth 1 err_result))))
  ); foreach
  (princ)
); c:macrobyblock|;


; Команда сохранения списка имён всех блоков рисунка в файл
;|(defun c:wbnames ( /
		  dictionary
		  group_list)

  ; Ввод имени файла
  (defun _inner_select ()
    (setq group_list (pimp_input_filename
		       pimp_msg_bg_filename
		       nil
		       group_list
		       nil
		       pimp_dlg_select_bg_filename
		       ""
		       1
		     ); pimp_input_filename
    ); setq
    (if (= group_list pimp_msg_exit)
      (exit)
    ); if
  ); _inner_select
  
  (setq dictionary (pimp_get_dict pimp_data_dict_name))
  (if dictionary
    (setq group_list (vlax-ldata-get dictionary pimp_xrecord_bg_filename nil))
  ); if
  ; Ввод имени файла или выход
  (if (vl-catch-all-error-p
          (vl-catch-all-apply '_inner_select nil)
      ); vl-catch-all-error-p
    (exit)
  ); if
  (princ "\nСохранение списка имён блоков... ")
  (pimp_export_block_names group_list)
  (princ "OK.")
  (princ)
); c:wbnames|;

; Команда сохранения на диск всех блоков документа, созданных пользователем
; Все блоки сохраняются в каталог текущего документа
(defun c:pimp_wab ( /
		    block_name
		    filename
		    wblock_dir
		    old_sysvar)
  (setq wblock_dir (vlax-get-property (pimp_get_active_doc) 'Path)
	old_sysvar (getvar "FILEDIA")
  ); setq
  ; Отключение окна запроса имени файла
  (setvar "FILEDIA" 0)
  ; Перебор всех блоков документа
  (vlax-for block (pimp_get_blocks)
    (setq block_name (vlax-get-property block 'Name))
    ; Сохранение блока на диск происходит, если его имя не "GENAXEH" и не начинается со знака "*"
    (if (and
	  (/= block_name "GENAXEH")
	  (/= (substr block_name 1 1) "*")
	); and
      (progn
	; Полное имя файла
        (setq filename (strcat wblock_dir "\\" block_name ".dwg"))
	; Проверка, существует ли уже такой файл
        (if (dos_filep filename)
          ; then
          (vl-cmdf "_.-WBLOCK" filename "_Y" block_name)
          ; else
          (vl-cmdf "_.-WBLOCK" filename block_name)
        ); if
      ); progn
    ); if
  ); vlax-for
  ; Включение режима вывода окна запроса имени файла по умолчанию
  (setvar "FILEDIA" old_sysvar)
  (princ)
); c:wball
(princ " OK.\n")
