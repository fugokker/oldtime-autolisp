; The file is encoded in ANSI CP-1251.
; Copyright 2005 by Muravev Kirill <fugokker@yandex.ru>. Licensed under CC BY-SA 4.0

; Модуль обеспечивает следующие действия:
;  - автоматическое задание стиля печати для вновь создаваемых слоёв
;    (только при использовании документом именованных стилей печати)
;  - автоматическое переключение на другой слой при выполнении некоторых команд
(princ "\nЗагрузка модуля PImpLayer...")
; Рабочий слой
(setq pimp_working_layer nil)
; Предыдущий слой
(setq pimp_prev_layer nil)
; Предыдущая команда
(setq pimp_prev_command nil)

; Список команд, требующих смены слоя
(setq pimp_sl_cmd_list (list
			 "ATTDEF"
	    
			 "DIMALIGNED"
			 "DIMLINEAR"
			 "DIMANGULAR"
			 "DIMARC"
			 "DIMBASELINE"
			 "DIMCENTER"
			 "DIMCONTINUE"
			 "DIMDIAMETER"
			 "DIMJOGGED"
			 "DIMORDINATE"
			 "DIMRADIUS"
			 "QDIM"
			 "QLEADER"
			 "TOLERANCE"

			 "POINT"
			 
			 "DTEXT"
			 "TEXT"
			 "MTEXT"

			 "BHATCH"
			 "HATCH"

			 "XATTACH"
			 "XREF"
		       ); list
); setq

; Удаление старых реакторов, если они есть
(foreach reactor_name (list
			'pimp_sl_cmd_reactor
			'pimp_sl_acdb_reactor
		      )
  (if (eval reactor_name)
    (progn
      (vlr-remove (eval reactor_name))
      (set reactor_name nil)
    ); progn
  ); if
); foreach

; Определяем командный реактор
(if (not pimp_sl_cmd_reactor)
  (setq pimp_sl_cmd_reactor (vlr-command-reactor
			      nil
			      '(
				(:vlr-commandWillStart . pimp_sl_cmd_start)
				(:vlr-commandEnded     . pimp_sl_cmd_end)
				(:vlr-commandCancelled . pimp_sl_cmd_end)
			      ); '
			    ); vlr-command-reactor
  ); setq
); if

; Определяем реактор базы данных
(if (not pimp_sl_acdb_reactor)
  (setq pimp_sl_acdb_reactor (vlr-acdb-reactor
			      T
			      '(
				(:vlr-objectAppended   . pimp_sl_acdb_append)
				(:vlr-objectReAppended . pimp_sl_acdb_append)
				(:vlr-objectUnErased   . pimp_sl_acdb_append)
				(:vlr-objectUnAppended . pimp_sl_acdb_delete)
				(:vlr-objectErased     . pimp_sl_acdb_delete)
			      ); '
			    ); vlr-acdb-reactor
  ); setq
); if


; Обработчик события добавления нового объекта в базу данных
; Процедура задания стиля печати для вновь добавляемых слоёв
; Ссылка на добавляемый слой заносится в список слоёв, для которых нужно задать стиль печати.
; Ссылка на слой добавляется в список, если:
;  - в документе используются именованные стили печати;
;  - стиль печати слоя не такой, как у слоя 0.
(defun pimp_sl_acdb_append (reactor
			    src_obj /
			    obj)
  (if (and
	(= (getvar 'PSTYLEMODE) 0)
	(vlr-data pimp_sl_acdb_reactor)
      ); and
    (progn
      ; Присвоение значения переменной obj с учётом возможности появления ошибки
      (setq obj (vl-catch-all-apply 'vlax-ename->vla-object (cdr src_obj)))
      (if (vl-catch-all-error-p obj)
	(setq obj nil)
      ); if
      ; Проверка наличия самого объекта
      (if obj
	; Проверка наличия у объекта полей, по которым можно проверить его тип
        (if (and
	      (vlax-property-available-p obj 'ObjectName)
	      (vlax-property-available-p obj 'PlotStyleName)
	    ); and
          ; Проверка типа добавленного объекта (слой или не слой)
          (if (and
	        (= (vlax-get-property obj 'ObjectName) 'AcDbLayerTableRecord)
	        (/= (vlax-get-property obj 'PlotStyleName) (getvar 'deflplstyle))
              ); and
	    ; then
	    ; Добавление ссылки на слой в список
	    (vlr-data-set
	      pimp_sl_cmd_reactor
	      (append
	        (vlr-data pimp_sl_cmd_reactor)
	        (list obj)
	      ); append
	    ); vlr-data-set
          ); if
        ); if
      ); if
    ); progn
  ); if
); pimp_sl_acdb_append


; Обработчик события удаления объекта из базы данных
; Процедура задания стиля печати для вновь добавляемых слоёв
; Исключение ссылки на удаляемый слой из списка слоёв, у которых нужно сменить стиль печати
(defun pimp_sl_acdb_delete (reactor
			    src_obj /
			    layers_list)
  (if (= (getvar 'PSTYLEMODE) 0)
    (progn
      (setq layers_list (vlr-data pimp_sl_cmd_reactor))
      (foreach layer layers_list
	(if (vlax-erased-p layer)
	  (setq layers_list (vl-remove layer layers_list))
	); if
      ); foreach
      (vlr-data-set pimp_sl_cmd_reactor layers_list)
    ); progn
  ); if
); pimp_sl_acdb_delete


; Обработчик события "перед началом команды"
(defun pimp_sl_cmd_start (reactor
			  command_info /
			  cmd_str)
  (setq cmd_str (vl-string-left-trim "-" (car command_info)))
  ; Логический флаг срабатывания обработчика события "объект удалён из базы даных"
  (vlr-data-set pimp_sl_acdb_reactor (/= cmd_str "LAYER"))
  ; Сброс имени запомненного рабочего слоя в случае, если вызываемая команда не является повтором предыдущей
  (if (/= cmd_str pimp_prev_command)
    (setq pimp_working_layer nil)
  ); if
  ; Рабочий слой меняется в том случае, если вызываемая команда находится в списке pimp_sl_cmd_list
  (if (member cmd_str pimp_sl_cmd_list)
    ; then
    (if pimp_working_layer
      (setvar 'CLAYER pimp_working_layer)
    ); if
    ; else
    ; Если вызываемой команды нет в списке pimp_sl_cmd_list, то имя рабочего слоя сбрасывается,
    ; а имя предыдущего слоя запоминается
    (setq pimp_prev_layer (getvar 'CLAYER)
	  pimp_working_layer nil
    ); setq
  ); if
  ; Сохранение имени вызываемой команды
  (setq pimp_prev_command cmd_str)
); pimp_sl_cmd_start


; Обработчик события отмены или завершения команды
; Процедура задания стиля печати для вновь добавляемых слоёв
(defun pimp_sl_cmd_end (reactor
			command_info /
			command_str
			layers_list)
  (setq command_str (vl-string-left-trim "-" (car command_info))
	layers_list (vlr-data pimp_sl_cmd_reactor)
  ); setq
  ; Если имя выполненной команды находится в списке pimp_sl_cmd_list, то:
  ;  - запоминается имя текущего (рабочего) слоя;
  ;  - текущий слой меняется на предыдущий запомненный
  (if (member command_str pimp_sl_cmd_list)
    ; then
    (progn
      (setq pimp_working_layer (getvar 'CLAYER))
      (if pimp_prev_layer
        (setvar 'CLAYER pimp_prev_layer)
      ); if
    ); progn
  ); if
  ; Задание стиля печати для слоёв, вновь добавленных в документ
  (foreach layer layers_list
    (vlax-put-property layer 'PlotStyleName (getvar 'deflplstyle))
  ); foreach
  ; Установка логических флагов срабатывания реакторов в начальное положение
  (vlr-data-set pimp_sl_cmd_reactor nil)
  (vlr-data-set pimp_sl_acdb_reactor T)
); pimp_sl_cmd_end
(princ " OK.\n")
