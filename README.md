# Набор приложений для AutoCAD Mechanical

Приложения Lisp из серии PImp (Project Improvements).

## Назначение

### 01-PImpGlobals.lsp

Содержит общие переменные и процедуры, используемые остальными приложениями серии. Должен быть загружен в AutoCAD первым, после библиотеки DOSLib ([см.](#зависимости)).

### 02-PImpLayer.lsp

Модуль обеспечивает:

- автоматическое задание стиля печати для вновь создаваемых слоёв (если в документе используются именованные стили печати);
- автоматическое переключение на другой слой при выполнении некоторых команд.

### 03-PImpBlock.lsp

Содержит команды для обработки блоков.

### 04-PImpXRef.lsp

Содержит команды для работы с внешними ссылками.

При вставке в документ файла DWG как внешней ссылки копирует её содержимое и вставляет поверх вставленной ссылки. Таким образом в документе создаётся подложка в виде объекта XRef и её контур, совпадающий с объектом и доступный для редактирования.

## Зависимости

Для работы приложений требуется [библиотека DOSLib](https://wiki.mcneel.com/doslib/home).

## Установка в AutoCAD

Файлы Lisp и ARX устанавливаются в окне "Загрузка/выгрузка приложений", вызываемом командой `_APPLOAD` либо из меню "Инструменты → Приложения..."

Для автоматической загрузки при каждом запуске AutoCAD приложения следует добавить в список автозагрузки (кнопка "Приложения..." в окне "Загрузка/выгрузка приложений").

Файлы приложений загружаются в алфавитном порядке, поэтому для корректной загрузки следует переименовать файл DOSLib так, чтобы он всегда располагался в начале списка загружаемых файлов.

_Пример: DOSLib24x64.arx → 000-DOSLib24x64.arx_

В общем случае загружаемые файлы должны быть выстроены в следующем порядке:

* 000-DOSLib24x64.arx
* 01-PImpGlobals.lsp
* 02-PImpLayer.lsp
* 03-PImpBlock.lsp
* 04-PImpXRef.lsp
